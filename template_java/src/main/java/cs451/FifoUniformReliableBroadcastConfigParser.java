package cs451;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.stream.Stream;

/**
 * FifoUniformReliableBroadcastConfigParser
 */
public class FifoUniformReliableBroadcastConfigParser {

    private Path configFilePath;
    private int howManyMessagesToSend;

    public boolean populate(String _configFilePath) {
        // This function is implemented in what is most likely a very redundant fashion.
        // Welp.

        configFilePath = Paths.get(_configFilePath);
        
        try {
            
            Stream<String> lines = Files.lines(configFilePath);
            String firstLine = lines.findFirst().get();
            lines.close();

            Scanner scan = new Scanner(firstLine);
            howManyMessagesToSend = scan.nextInt();
            scan.close();

        } catch (IOException exc) {
            System.err.println("No bueno, something happened when we tried to open the config file :(");
            System.err.println(exc.toString());
            return false;
        }

        return true;
    }

    public int getHowManyMessagesToSend() {
        return howManyMessagesToSend;
    }
}
