package cs451;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;


public class MultiShotLatticeAgreementConfigParser {

    private Path configFilePath;
    private int maxPerProposal;
    private int maxAcrossAllProposals;
    private List<Set<Integer>> proposalsToDo;

    public boolean populate(String _configFilePath) {
        // This function is implemented in what is most likely a very redundant fashion.
        // Welp.

        configFilePath = Paths.get(_configFilePath);
        
        try {
            
            List<String> lines = Files.lines(configFilePath).collect(Collectors.toList());

            Scanner scan = new Scanner(lines.get(0));
            short howManyProposals = scan.nextShort();
            maxPerProposal = scan.nextInt();
            maxAcrossAllProposals = scan.nextInt();
            scan.close();

            proposalsToDo = new LinkedList<>();
            for (int i = 0; i < howManyProposals; i++) {
                Scanner scan2 = new Scanner(lines.get(i + 1));
                Set<Integer> thisProposal = new HashSet<>();
                thisProposal.add(scan2.nextInt());
                while (scan2.hasNextInt()) {
                    thisProposal.add(scan2.nextInt());
                }
                proposalsToDo.add(thisProposal);
            }

        } catch (IOException exc) {
            System.err.println("No bueno, something happened when we tried to open the config file :(");
            System.err.println(exc.toString());
            return false;
        }

        return true;
    }

    public int getMaxPerProposal() {
        return maxPerProposal;
    }

    public int getMaxAcrossAllProposals() {
        return maxAcrossAllProposals;
    }

    public List<Set<Integer>> getProposalsToDo() {
        return proposalsToDo;
    }
}
