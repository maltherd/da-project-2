package cs451;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    
    private static class SignalHandlingThread extends Thread {
        private Collection<Logger> loggers;
        private String logFilePath;

        public SignalHandlingThread(Collection<Logger> _loggers, String _logFilePath) {
            loggers = _loggers;
            logFilePath = _logFilePath;
        }

        @Override
        public void run() {
           // immediately stop network packet processing
            System.out.println("Immediately stopping network packet processing.");

            // write/flush output file if necessary
            System.out.println("Writing output.");
            
            try {
                for (Logger l : loggers) {
                    l.flushToFile(logFilePath);
                }
            } catch (IOException exc) {
                System.err.println("Oh no, there was IOException when flushing output :(");
                System.err.println(exc.toString());
            }
        }
    }

    private static void initSignalHandlers(Collection<Logger> loggers, String logFilePath) {
        Runtime.getRuntime().addShutdownHook(new SignalHandlingThread(loggers, logFilePath));
    }

    public static void main(String[] args) throws InterruptedException {
        Parser parser = new Parser(args);
        parser.parse();

        Collection<Logger> loggers = new LinkedList<>(); // this list will grow → use LinkedList instead of ArrayList
        initSignalHandlers(loggers, parser.output());

        // example
        long pid = ProcessHandle.current().pid();
        System.out.println("My PID: " + pid + "\n");
        System.out.println("From a new terminal type `kill -SIGINT " + pid + "` or `kill -SIGTERM " + pid + "` to stop processing packets\n");

        System.out.println("My ID: " + parser.myId() + "\n");
        System.out.println("List of resolved hosts is:");
        System.out.println("==========================");
        for (Host host: parser.hosts()) {
            System.out.println(host.getId());
            System.out.println("Human-readable IP: " + host.getIp());
            System.out.println("Human-readable Port: " + host.getPort());
            System.out.println();
        }
        System.out.println();

        System.out.println("Path to output:");
        System.out.println("===============");
        System.out.println(parser.output() + "\n");

        System.out.println("Path to config:");
        System.out.println("===============");
        System.out.println(parser.config() + "\n");

        System.out.println("Doing some initialization\n");

        System.out.println("Broadcasting and delivering messages...\n");

        // END OF GIVEN CODE - START OF MY CODE
        
        // Preparation for the setup - aka wrangling the things I need out of the given parser.
        Host me = parser.hosts().stream().filter(h -> h.getId() == parser.myId()).findFirst().get();
        Set<Host> notMe = parser.hosts().stream().filter(h -> h.getId() != parser.myId()).collect(Collectors.toSet());
        Set<Host> allHosts = notMe;
        allHosts.add(me);
       
        // Parser
        MultiShotLatticeAgreementConfigParser mcp = new MultiShotLatticeAgreementConfigParser();
        if (!mcp.populate(parser.config())) {
            System.exit(1);
        }

        // Actual setup
        NetworkMuxDemux layer0; 
        try {
            layer0 = new NetworkMuxDemux(me, false, false);
        } catch (IOException exc) {
            System.err.println("No bueno, an exception occured when we tried to either select/configureBlocking/bind/register the socket :(");
            System.err.println(exc.toString());
            System.exit(1);
            return;  // this return is never reached, but it keeps my language server from warning avout layer0 being uninitialized.
        }

        MultiShotLatticeAgreementLayer layer3 = new MultiShotLatticeAgreementLayer(allHosts, (short)mcp.getMaxPerProposal(), true);
        loggers.add(layer3);

        DemuxMuxLayer<byte[], Host> layer2 = new DemuxMuxLayer<>(layer3);
        
        // Create one PerfectLinkLayer for each host.
        allHosts.stream().forEach((remote) -> {
            PerfectLinkLayer pll = new PerfectLinkLayer(remote, layer2, false);

            layer0.registerLayerOnTop(pll, remote.asSocketAddress());
            layer2.registerBottomLayer(pll, remote);
        });

        // Do them proposes.
        mcp.getProposalsToDo().stream().forEachOrdered((set) -> {
            layer3.propose(set); 
        });

        // After a process finishes broadcasting,
        // it waits forever for the delivery of messages.
        while (true) {
            try {
                layer0.work(); 
            } catch (IOException exc) {
                System.err.println("No bueno, an exception occured when we tried to either select/send/receive :(");
                System.err.println(exc.toString());
                System.exit(1);
                return;
            }
        }
    }
}
