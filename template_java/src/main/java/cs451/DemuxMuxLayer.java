package cs451;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * DemuxMuxLayer
 *
 * <p>This layer is designed to have multiple layers under it; and only one on top of it.
 * It does the required routing of delivery events and send opportunities.
 * Routing is all-to-one going up, and one-to-one going down depending on the registered address.
 * This layer is intended as a dumb passthrough.</p>
 *
 * <p>I had to create this class because `UniformReliableBroadcastLayer` does not, in principle, need to have
 * multiple `Layer`s under it (`PerfectLinkLayer`s, that is). So, that class (as with every `Layer` but this one, in fact!) 
 * is designed assuming that there is only one layer under it.</p>
 *
 * <p>This assumption breaks down when multiple underlayers call `UniformReliableBroadcastLayer`s
 * `handleSendToBottomOpportunity()` method. It is indeed the case, since there are multiple `PerfectLinkLayer`s under it.</p>
 *
 * <p>So, I made this class as a compromise to keep the rest of the layers "generic" enough.</p>
 *
 * <p>Maybe it would have been cleaner to assume, for all `Layer`s, that they have multiple underlayers, and have all `Layer`s
 * be capable of some sort of routing. But, it would definitely have been more complicated to understand. So I'm happy with this
 * tradeoff.</p>
 */
public class DemuxMuxLayer<BC, BA> implements Layer<BC, BC, BA> {
    
    protected Map<BA, Layer<?, BC, ?>> bottomLayers;
    protected Layer<BC, ?, BA> layerOnTop;

    public DemuxMuxLayer(Layer<BC, ?, BA> _layerOnTop) {
        layerOnTop = _layerOnTop;
        bottomLayers = new TreeMap<>();
    }

    public void registerBottomLayer(Layer<?, BC, ?> newBottomLayer, BA routingAddress) {
        bottomLayers.put(routingAddress, newBottomLayer);
    }

    @Override
    public Map<BA, List<BC>> handleSendToBottomOpportunity() {
        // Here is the crux of the dinguerie.
        // We inject the messages directly in the bottom layers.
        Map<BA, List<BC>> messagesToPassDown = layerOnTop.handleSendToBottomOpportunity();
        messagesToPassDown.entrySet().stream()
            .forEach(e -> {
                e.getValue().stream()
                    .forEach(bc -> bottomLayers.get(e.getKey()).broadcast(bc));
            });

        // Return nothing to be handled in the classic way by the bottom guys.
        return new HashMap<>();
    }

    @Override
    public void handleDeliveryFromBottom(BC bottomMessage, BA bottomAddressSource) {
        // Simply forward up.
        layerOnTop.handleDeliveryFromBottom(bottomMessage, bottomAddressSource);
    }

    @Override
    public void broadcast(BC content) {
        throw new RuntimeException("DemuxMuxLayer is only a passthrough ! :(");
    }
}
