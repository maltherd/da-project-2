package cs451;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Logger
 */
public class Logger {
    StringBuffer buf;

    public Logger() {
        buf = new StringBuffer(1024);
    }

    public void logDelivery(LoggableMessage message, int sourceId) {
        String logLine = String.format("d %d %d\n", sourceId, message.id());
        buf.append(logLine);
    }

    public void logDecision(String decision) {
        String logLine = String.format("%s\n", decision);
        buf.append(logLine);
    }

    public void logBroadcast(LoggableMessage message) { 
        String logLine = String.format("b %d\n", message.id());
        buf.append(logLine);
    }

    public void flushToFile(String logFilePath) throws IOException {
        Path logFile = Paths.get(logFilePath);
        OpenOption[] options = new OpenOption[] { StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE };
        FileChannel logChannel = FileChannel.open(logFile, options);
        
        logChannel.write(ByteBuffer.wrap(buf.toString().getBytes())); // convoluted as fuck
        logChannel.close(); 
    }
}
