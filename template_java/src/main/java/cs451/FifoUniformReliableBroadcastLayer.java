package cs451;

import java.util.Set;

/**
 * FifoUniformReliableBroadcastLayer
 */
public class FifoUniformReliableBroadcastLayer extends UniformReliableBroadcastLayer {

    public FifoUniformReliableBroadcastLayer(Host _me, Set<Host> _allRemoteHosts, LayerWithoutAddressBelow<BroadcastMessage> _layerOnTop, boolean _printDebug) {
        super(_me, _allRemoteHosts, _layerOnTop, _printDebug);
    }

    @Override
    protected boolean canDeliver(GlobalSequenceNumber m) {
        GlobalSequenceNumber prev = new GlobalSequenceNumber();
        prev.originatorId = m.originatorId;
        prev.localSequenceNumber = m.localSequenceNumber - 1;

        return hasMajorityAck(m) && (delivered.contains(prev) || m.localSequenceNumber == 1);
    }
}
