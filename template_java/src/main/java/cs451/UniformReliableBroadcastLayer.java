package cs451;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * UniformReliableBroadcastLayer
 *
 * We use here algorithm 3.5 of the book "Majority-Ack Uniform Reliable Broadcast".
 */
public class UniformReliableBroadcastLayer extends Logger implements Layer<ByteBuffer, ByteBuffer, Host> {

    public static class GlobalSequenceNumber implements Comparable<GlobalSequenceNumber> {
        public int originatorId;
        public int localSequenceNumber;

        public int compareTo(GlobalSequenceNumber other) {
            if (localSequenceNumber == other.localSequenceNumber) {
                return (originatorId - other.originatorId);
            }
            return (localSequenceNumber - other.localSequenceNumber);
        }

        public String toString() {
            return "gsn(" + originatorId + ":" + localSequenceNumber + ")";
        }
    }

    public static class BroadcastMessage implements LoggableMessage {
        public GlobalSequenceNumber sequence;
        public ByteBuffer content;

        @Override
        public int id() {
            return sequence.localSequenceNumber;
        }

        public ByteBuffer ser() {
            ByteBuffer ret = ByteBuffer.allocate(4 + 4 + 1 + content.capacity());
            ret.putInt(sequence.originatorId).putInt(sequence.localSequenceNumber).putChar((char)content.capacity()).put(content);
            return ret;
        }

        public static BroadcastMessage deser(ByteBuffer buf) {
            BroadcastMessage ret = new BroadcastMessage();

            ret.sequence = new GlobalSequenceNumber(); 
            ret.sequence.originatorId = buf.getInt();
            ret.sequence.localSequenceNumber = buf.getInt();

            char size = buf.getChar();
            byte[] arr = new byte[size];
            buf.get(arr, 0, size);
            ret.content = ByteBuffer.wrap(arr);

            return ret;
        }
    }

    protected TreeSet<GlobalSequenceNumber> delivered; // TODO: Optimize memory here
    protected TreeSet<GlobalSequenceNumber> pending; // Cannot optimize here :(
    protected TreeMap<GlobalSequenceNumber, Set<Host>> ack; // TODO: Optimize memory here

    protected TreeMap<GlobalSequenceNumber, BroadcastMessage> sendQueue;
    protected int nextLocalSequenceNumberForMeToSend;

    protected Set<Host> allRemoteHosts;
    protected Host me;

    protected LayerWithoutAddressBelow<BroadcastMessage> layerOnTop;

    protected boolean printDebug;

    public UniformReliableBroadcastLayer(Host _me, Set<Host> _allRemoteHosts, LayerWithoutAddressBelow<BroadcastMessage> _layerOnTop, boolean _printDebug) {
        me = _me;
        allRemoteHosts = _allRemoteHosts;
        layerOnTop = _layerOnTop;
        
        delivered = new TreeSet<>();
        pending = new TreeSet<>();
        ack = new TreeMap<>();

        sendQueue = new TreeMap<>();
        nextLocalSequenceNumberForMeToSend = 1;  // Reee...

        printDebug = _printDebug;
    }

    protected boolean hasMajorityAck(GlobalSequenceNumber m) {
        // ack only contains retransmits from the others :)
        if (ack.containsKey(m)) {
            boolean ok = (ack.get(m).size() >= (allRemoteHosts.size()/ 2));

            if (printDebug) {
                if (!ok) {
                    System.err.print("There are only " + ack.get(m).size() + " people who acked " + m + "\n");
                } else {
                    System.err.print("Majority ok for " + m + "\n");
                }
            }

            return ok;
        }

        if (printDebug) {System.err.print("No one acked" + m + "\n");}

        return false;
    }

    protected boolean canDeliver(GlobalSequenceNumber m) {
        return hasMajorityAck(m);
    }

    public void broadcast(ByteBuffer content) {
        // Craft a message.
        BroadcastMessage msg = new BroadcastMessage();
        msg.sequence = new GlobalSequenceNumber();
        msg.sequence.originatorId = me.getId();
        msg.sequence.localSequenceNumber = nextLocalSequenceNumberForMeToSend++;
        msg.content = content;
        
        // Put it in the sendQueue.
        sendQueue.put(msg.sequence, msg);

        // Log broadcast. For grading purposes.
        logBroadcast(msg);
    }

    @Override
    public void handleDeliveryFromBottom(ByteBuffer bottomContent, Host bottomAddressSource) {
        
        // Parse the message
        BroadcastMessage message = BroadcastMessage.deser(bottomContent);
        
        // Put this (re)transmit in `ack`
        Set<Host> sett = ack.getOrDefault(message.sequence, new TreeSet<>());
        sett.add(bottomAddressSource);
        ack.put(message.sequence, sett);

        // If the message is not pending, schedule it for rebroadcasting
        if (!pending.contains(message.sequence)) {
            sendQueue.put(message.sequence, message);
        }
        
        // If the message can be delivered, do it.
        if (canDeliver(message.sequence) && !delivered.contains(message.sequence)) {
            delivered.add(message.sequence);
            if (layerOnTop != null) {
                layerOnTop.handleDeliveryFromBottom(message); 
            }
            logDelivery(message, message.sequence.originatorId);
        }

    }

    @Override
    public Map<Host, List<ByteBuffer>> handleSendToBottomOpportunity() {
        Map<Host, List<ByteBuffer>> toSend = new HashMap<>();

        if (!sendQueue.isEmpty()) {

            // Broadcast the messages to everyone.
            sendQueue.entrySet().stream()
                .forEach((e) -> {
                    
                    // Schedule for sending
                    allRemoteHosts.stream()
                        .forEach((h) -> {
                            List<ByteBuffer> ls = toSend.getOrDefault(h, new ArrayList<>());
                            ls.add(e.getValue().ser());
                            toSend.put(h, ls);
                        });

                    // Put the message in `pending`
                    pending.add(e.getKey());

                });

            // Clear the queue
            sendQueue.clear();

        }

        return toSend;
    }
    
}
