package cs451;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.StandardProtocolFamily;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * NetworkMuxDemux
 *
 * This class sits between a network channel and multiple layers.
 * It does the routing based on the SocketAddress of packets.
 **/
public class NetworkMuxDemux {
    protected Host me;
    protected Selector selector;

    protected Map<UUID, Layer<byte[], ?, SocketAddress>> layersOnTop;
    protected Map<SocketAddress, UUID> demuxTable;

    public static final int MAX_MESSAGE_SIZE = 128;
    private boolean printDebug;
    private boolean printDebugSelective;
    private SocketAddress printDebugAddress;

    public NetworkMuxDemux(Host _me, boolean _printDebug, boolean _printDebugSelective) throws IOException {
        me = _me;
        selector = Selector.open();

        DatagramChannel channel = DatagramChannel.open(StandardProtocolFamily.INET);
        channel.configureBlocking(false);
        channel.socket().bind(me.asSocketAddress());
        channel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);

        layersOnTop = new HashMap<>();
        demuxTable = new LinkedHashMap<>();

        printDebug = _printDebug;
        printDebugSelective = _printDebugSelective;
        printDebugAddress = null;
    }

    private void printDebugSend(SocketAddress dest, byte[] arr) {
        if (!printDebug) {return;}
        if (printDebugSelective && !dest.equals(printDebugAddress)) {return;}
        System.err.println(
            Instant.now().toString().split("T")[1] 
            + " sent:" + ((InetSocketAddress)dest).getPort() 
            + " len:" + arr.length
            + " " + Arrays.toString(arr)
        );
    }

    private void printDebugReceive(SocketAddress src, byte[] arr) {
        if (!printDebug) {return;}
        if (printDebugSelective && !src.equals(printDebugAddress)) {return;}
        System.err.println(
            Instant.now().toString().split("T")[1] 
            + " rcvd:" + ((InetSocketAddress)src).getPort() 
            + " len:" + arr.length
            + " " + Arrays.toString(arr)
        );
    }

    public UUID registerLayerOnTop(Layer<byte[], ?, SocketAddress> layer, SocketAddress routingRemoteAdress) {
        UUID uuid = UUID.randomUUID();
        layersOnTop.put(uuid, layer);
        demuxTable.put(routingRemoteAdress, uuid);

        if (printDebugSelective && printDebugAddress == null) {
            printDebugAddress = routingRemoteAdress;
        }

        return uuid;
    }

    public void work() throws IOException {
        selector.select();

        for (SelectionKey sk : selector.selectedKeys()) {
            if (sk.isReadable()) {
                ByteBuffer b = ByteBuffer.allocate(MAX_MESSAGE_SIZE);
                DatagramChannel c = (DatagramChannel) sk.channel();
                SocketAddress source = c.receive(b);

                if (source != null) {
                    byte[] content = b.array();
                    printDebugReceive(source, content);
                    layersOnTop.get(demuxTable.get(source)).handleDeliveryFromBottom(content, source);
                }
            }

            if (sk.isWritable()) {
                for (Layer<byte[], ?, SocketAddress> layer : layersOnTop.values()) {
                    Map<SocketAddress, List<byte[]>> messagesToSend = layer.handleSendToBottomOpportunity();
                    
                    // This ↓↓ for loop should only ever iterate once.
                    // Each layer should have only one possible SocketAddress to send to.
                    // But well, let's keep things decoupled :)
                    for (Map.Entry<SocketAddress, List<byte[]>> entry : messagesToSend.entrySet()) {
                        
                        for (byte[] msg : entry.getValue()) {
                            int length = Math.min(MAX_MESSAGE_SIZE, msg.length);
                            ByteBuffer b = ByteBuffer.allocate(length);
                            DatagramChannel c = (DatagramChannel) sk.channel();
                            
                            b.put(msg);
                            b.flip();
                            printDebugSend(entry.getKey(), msg);
                            c.send(b, entry.getKey()); // TODO: check return
                        }
                    }
                }
            }
        }

        return;
    }
}
