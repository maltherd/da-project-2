package cs451;

import java.util.List;

/** 
 * M is the type of message delivered and sent by the layer below this one.
 */
public interface LayerWithoutAddressBelow<M> {

    /** The bottom layer will call this when it has a message to deliver to this layer. */
    public void handleDeliveryFromBottom(M bottomMessage);
    
    /** The bottom layer will call this when it is ready to send messages for this layer */
    public List<M> handleSendToBottomOpportunity();

}

