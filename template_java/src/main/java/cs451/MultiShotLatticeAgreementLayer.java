package cs451;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * MultiShotLatticeAgreement
 */
public class MultiShotLatticeAgreementLayer extends Logger implements Layer<byte[], byte[], Host> {
    
    public static class Message {

        public Message(int _shotSeq, int _proposalSeq, Type _type, Set<Integer> _proposal) {
            shotSeq = _shotSeq;
            proposalSeq = _proposalSeq;
            type = _type;
            proposal = (_proposal == null ? new HashSet<>() : _proposal);
        }

        public enum Type {
            PROPOSAL, ACK, NACK, BAD
        };

        public Type type;
        public Set<Integer> proposal;
        public int proposalSeq;
        public int shotSeq;

        public static short maxProposalSize;

        public byte[] ser() {
            ByteBuffer buf = ByteBuffer.allocate(1 + 4 + 4 + 2 + (proposal.size() * 4));

            buf.put(type == Type.PROPOSAL ? (byte)'P' : type == Type.ACK ? (byte)'A' : (byte)'N');
            buf.putInt(shotSeq).putInt(proposalSeq).putShort((short)proposal.size());
            proposal.stream()
                .forEach((i) -> {
                    buf.putInt(i);
                });
            
            return buf.array();
        }
       
        public static Message deser(byte[] arr) {
            ByteBuffer buf = ByteBuffer.wrap(arr);

            char typeChar = (char)buf.get();
            Type type = typeChar == 'P' ? Type.PROPOSAL : typeChar == 'A' ? Type.ACK : typeChar == 'N' ? Type.NACK : Type.BAD;

            if (type == Type.BAD) {
                throw new RuntimeException("Bad Lattice message: " + Arrays.toString(buf.array()));
            }
            
            int shotSeq = buf.getInt();
            int proposalSeq = buf.getInt();
            
            short proposalSize = buf.getShort();
            Set<Integer> proposal = new HashSet<>();
            for (int i = 0; i < proposalSize; i++) {
                proposal.add(buf.getInt());
            }
            
            return new Message(shotSeq, proposalSeq, type, proposal);
        }

        @Override
        public String toString() {
            return "[" + type.toString() + "] S=" + shotSeq + " P=" + proposalSeq + " " + proposal.toString();
        }
    }

    protected boolean printDebug;

    protected int currentShotSeq;
    protected List<Set<Integer>> shotQueue;
    protected Map<Integer, Set<Integer>> decisionHistory;
    protected boolean active; 

    protected Set<Integer> currentProposedValue;
    protected Set<Integer> currentAcceptedValue;
    protected int currentProposalSeq;
    protected int ackCount;
    protected int nackCount;

    protected Map<Host, List<Message>> sendQueue;
    protected List<Message> broadcastQueue;
    protected Set<Host> allHosts;
 
    public MultiShotLatticeAgreementLayer(Set<Host> _allHosts, short maxProposalSize, boolean _printDebug) {
        printDebug = _printDebug;

        currentShotSeq = 0;
        shotQueue = new LinkedList<>();
        decisionHistory = new HashMap<>();
        active = false;

        currentProposedValue = new HashSet<>();
        currentAcceptedValue = new HashSet<>();
        currentProposalSeq = 0;
        ackCount = 0;
        nackCount = 0;

        sendQueue = new HashMap<>();
        broadcastQueue = new ArrayList<>();
        allHosts = _allHosts;

        Message.maxProposalSize = maxProposalSize;  // Crazy i know.
    }

    private void debug(String message) {
        if (printDebug) {
            System.err.println(message);
        }
    }

    @Override
    public Map<Host, List<byte[]>> handleSendToBottomOpportunity() {

        // Try to dequeue a proposal if we are inactive;
        if (!shotQueue.isEmpty() && !active) {
            debug("New shot: " + (currentShotSeq + 1));

            active = true;
            Set<Integer> proposal = shotQueue.remove(0);

            currentProposedValue = proposal;
            currentAcceptedValue.clear();
            currentShotSeq++;
            currentProposalSeq = 1;
            ackCount = 0;
            nackCount = 0;

            Message m = new Message(currentShotSeq, currentProposalSeq, Message.Type.PROPOSAL, proposal);
            broadcastQueue.add(m);
        }

        // See what we have to send.
        Map<Host, List<byte[]>> toSend = new HashMap<>();
        if (!sendQueue.isEmpty() || !broadcastQueue.isEmpty()) {

            // Broadcast messages.
            broadcastQueue.stream()
                .forEach((m) -> {
                    debug("    cast " + m.toString());

                    // Schedule for sending
                    allHosts.stream()
                        .forEach((h) -> {
                            List<byte[]> ls = toSend.getOrDefault(h, new ArrayList<>());
                            ls.add(m.ser());
                            toSend.put(h, ls);
                        });

                });

            // Send direct messages.
            sendQueue.entrySet().stream()
                .forEach((e) -> {
                    
                    // Schedule for sending
                    List<byte[]> ls = toSend.getOrDefault(e.getKey(), new ArrayList<>());
                    
                    e.getValue().stream()
                        .forEach((m) -> {
                            ls.add(m.ser());
                            toSend.put(e.getKey(), ls);
                            debug("    send " + m.toString() + " to " + e.getKey().getId());
                        });

                    toSend.put(e.getKey(), ls);

                });

            // Clear the queues
            broadcastQueue.clear();
            sendQueue.clear();

        }

        return toSend;
    }

    @Override
    public void handleDeliveryFromBottom(byte[] belowContent, Host bottomAddressSource) {
        Message m = Message.deser(belowContent);
        
        debug("    recv " + m.toString() + " from " + bottomAddressSource.getId());

        Message response;

        if (m.type == Message.Type.PROPOSAL) {
            // Handle PROPOSALs

            if (m.shotSeq == currentShotSeq) {
                // New proposals

                if (m.proposal.containsAll(currentAcceptedValue)) {
                    currentAcceptedValue = m.proposal;
                    response = new Message(m.shotSeq, m.proposalSeq, Message.Type.ACK, null);
                } else {
                    currentAcceptedValue.addAll(m.proposal);
                    response = new Message(m.shotSeq, m.proposalSeq, Message.Type.NACK, currentAcceptedValue);
                }

            } else if (m.shotSeq < currentShotSeq) {
                // Help late processes.

                Set<Integer> pastDecision = decisionHistory.get(m.shotSeq);

                if (m.proposal.containsAll(pastDecision)) {
                    response = new Message(m.shotSeq, m.proposalSeq, Message.Type.ACK, null);
                } else {
                    response = new Message(m.shotSeq, m.proposalSeq, Message.Type.NACK, pastDecision);
                }

            } else {
                // Fuck fast processes.
                response = new Message(m.shotSeq, m.proposalSeq, Message.Type.NACK, null);
            }

            List<Message> ls = sendQueue.getOrDefault(bottomAddressSource, new ArrayList<>());
            ls.add(response);
            sendQueue.put(bottomAddressSource, ls);
            
        } else if (m.shotSeq == currentShotSeq) {
            // Handle ACKs and NACKs

            if (m.type == Message.Type.ACK && m.proposalSeq == currentProposalSeq) {
                debug("ACK counted");
                ackCount++;
            } else if (m.type == Message.Type.NACK && m.proposalSeq == currentProposalSeq) {
                debug("NACK counted");
                currentProposedValue.addAll(m.proposal);
                nackCount++;
            }

            int majority = (int)Math.ceil(allHosts.size() / 2) + 1;
            
            if (active && (nackCount > 0) && (ackCount + nackCount >= majority)) {
                debug(
                    "Shot " + currentShotSeq 
                    + " Proposal " + currentProposalSeq 
                    + " Repropose (nack=" + nackCount + " ack=" + ackCount + ")"
                );

                currentProposalSeq++;
                ackCount = 0;
                nackCount = 0;

                response = new Message(currentShotSeq, currentProposalSeq, Message.Type.PROPOSAL, currentProposedValue);
                broadcastQueue.add(response);

            } else if (active && (ackCount >= majority)) {
                debug(
                    "Shot " + currentShotSeq 
                    + " Proposal " + currentProposalSeq 
                    + " Decide " + currentProposedValue.toString()
                );

                decisionHistory.put(currentShotSeq, currentProposedValue);
                decide(currentProposedValue);
                active = false;

            } else {
                debug(
                    "Shot " + currentShotSeq 
                    + " Proposal " + currentProposalSeq 
                    + " Cannot decide/repropose yet (nack=" + nackCount + " ack=" + ackCount + "/" + majority + ")"
                );

            }
        }
        
    }

    public void propose(Set<Integer> proposal) {
        debug("Proposal " + proposal.toString());
        shotQueue.add(proposal);
    }

    public void decide(Set<Integer> decision) {

        StringBuilder sb = new StringBuilder();
        decision.stream()
            .forEach((x) -> {
                sb.append(x);
                sb.append(" ");
            });

        logDecision(sb.toString());
    }

    @Override
    public void broadcast(byte[] content) {
        throw new UnsupportedOperationException("Not a broadcasting layer. Tell Arnaud to rework his class hierarchy.");
    }
}
