package cs451;

import java.util.List;
import java.util.Map;

/** 
 * <p>Note that the content types are not equal to the message types! Message = content + headers.</p>
 */
public interface Layer<BelowLayerContent, ThisLayerContent, BelowLayerAddress> {

    /** The bottom layer will call this when it has a message to deliver to this layer. */
    public void handleDeliveryFromBottom(BelowLayerContent belowContent, BelowLayerAddress bottomAddressSource);
    
    /** The bottom layer will call this when it is ready to send messages for this layer */
    public Map<BelowLayerAddress, List<BelowLayerContent>> handleSendToBottomOpportunity();

    public void broadcast(ThisLayerContent content);

    public static class NoContent {};

}

