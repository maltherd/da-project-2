package cs451;

import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.HashMap;

/**
 * PerfectLinkLayer
 */
public class PerfectLinkLayer extends Logger implements Layer<byte[], byte[], SocketAddress> {
    
    public static class PlMessage implements LoggableMessage {
        public enum Type {
            SEQ, ACK, BAD
        };

        // TODO: optimize size of ints to shorts; optimize type to a bit?
        public int sequence;
        public Instant timeout;
        public Type type;
        public byte[] content;  // Assuming content is at most 128 bytes long. (not enforced in code except deser(). enjoy !)

        @Override
        public int id() {
            return sequence;
        }

        public byte[] ser() {
            ByteBuffer buf;
            if (type == Type.SEQ) {
                buf = ByteBuffer.allocate(1 + 4 + 1 + content.length);
                buf.put((byte)'S').putInt(sequence).put((byte)content.length).put(content);
            } else {
                buf = ByteBuffer.allocate(1 + 4);
                buf.put((byte)'A').putInt(sequence);
            }
            return buf.array();
        }

        public static PlMessage deser(byte[] arr) {
            PlMessage ret = new PlMessage();
            ByteBuffer buf = ByteBuffer.wrap(arr);

            char typeChar = (char)buf.get();
            ret.type = typeChar == 'S' ? Type.SEQ : typeChar == 'A' ? Type.ACK : Type.BAD;

            if (ret.type == Type.BAD) {
                throw new RuntimeException("Bad PL message: " + Arrays.toString(arr));
            }
            
            ret.sequence = buf.getInt();
            if (ret.type == Type.SEQ) {
                byte size = buf.get();
                byte[] sub_arr = new byte[size];
                buf.get(sub_arr, 0, size);
                ret.content = sub_arr;
            }

            return ret;
        }
    }

    public enum CongestionControlState {
        SLOW_START, CONGESTION_AVOIDANCE
    };

    protected Host remote;

    protected Layer<byte[], ?, Host> layerOnTop;

    protected boolean printDebug;

    private void debug(String message) {
        if (printDebug) {
            System.err.println(remote.getId() + ") " + message);
        }
    }

    // Stream coming in
    // TODO: optimize to shorts + add countermeasures for wrapping around 65535;
    protected int lastSeqReceivedByMe; // May be renamed `lastSeqReceivedAndAlsoDeliveredByMe`. More explicit. But too long.
    protected int lastAckSentByMe; // should always be equal to the above.
    protected int receivingWindowSize; // = max allowed delta between lastSeqReceivedByMe and lastAckSentByMe. Bigger is better! --- NOT used currently.
    protected Instant lastAckSentByMeTimestamp;
    protected Duration sendingAckCooldown;
    protected boolean sendingAckAllowed;
    protected TreeMap<Integer, PlMessage> outstandingSeqsUnackedByMe; // its size is at most receivingWindowSize --- ACTUALLY not enforced (yet).

    // Stream going out
    // -- Congestion stuff
    protected CongestionControlState ccState;
    protected int ssThresh;
    // -- Regular stuff
    protected int lastSeqSentByMe;
    protected int lastAckReceivedByMe;
    protected byte repetitionsOfSameAck;
    protected int sendingWindowSize; // = max allowed delta between lastSeqSentByMe and lastAckReceivedByMe. Bigger is not always better (losses happen in groups!).
    protected Duration initialSendingSeqTimeout;
    protected Duration sendingSeqTimeout;
    protected TreeMap<Integer, PlMessage> outstandingSeqsUnackedByThem; // its size is at most sendingWindowSize

    public PerfectLinkLayer(Host _remote, Layer<byte[], ?, Host> _layerOnTop, boolean _printDebug) {
        printDebug = _printDebug;

        remote = _remote;
        layerOnTop = _layerOnTop;
        
        lastSeqReceivedByMe = 0;  // Damn project guys want the first SEQ to be 1... smh
        lastAckSentByMe = 0;
        receivingWindowSize = 1024;
        lastAckSentByMeTimestamp = Instant.EPOCH;
        sendingAckCooldown = Duration.ofMillis(30);
        sendingAckAllowed = false;
        outstandingSeqsUnackedByMe = new TreeMap<>();

        ccState = CongestionControlState.SLOW_START;
        ssThresh = 32;
        lastSeqSentByMe = 0;
        lastAckReceivedByMe = 0;
        repetitionsOfSameAck = 0;
        sendingWindowSize = 16;
        initialSendingSeqTimeout = Duration.ofMillis(5000);  // Must be big enough compared to sendingAckCooldown
        sendingSeqTimeout = initialSendingSeqTimeout;
        outstandingSeqsUnackedByThem = new TreeMap<>();
    }
    
    private void penalize(int timedOutAck) {
        // This function is meant to be called after every timeout event.
        ccState = CongestionControlState.SLOW_START;
        ssThresh = Math.max(1, sendingWindowSize / 2);
        sendingWindowSize = Math.max(1, sendingWindowSize / 3); // normally we set to 1. but it's too slow.
    }

    private void penalizeLightly(int missingAck) {
        // This function is meant to be called when 3 duplicate ACKs arrive.
        ccState = CongestionControlState.CONGESTION_AVOIDANCE;
        ssThresh = Math.max(1, sendingWindowSize / 2);
        sendingWindowSize = Math.max(1, sendingWindowSize / 2);
    }

    private void encourage() {
        // This function is meant to be called after every successful round-trip.       
        if (ccState == CongestionControlState.SLOW_START) {
           sendingWindowSize *= 2;
           if (sendingWindowSize > ssThresh) {
                ccState = CongestionControlState.CONGESTION_AVOIDANCE;
           }
        } else if (ccState == CongestionControlState.CONGESTION_AVOIDANCE) {
            sendingWindowSize += 1;
        }

        if (sendingWindowSize > ssThresh * 4) {
            // Network feels better than before?
            // Increase the threshold.
            ssThresh *= 2;
        }
    }

    @Override
    public void broadcast(byte[] content) {
        // This method would be better named `send` but inheritance is annoying.

        // Craft a SEQ message.
        PlMessage msg = new PlMessage();

        msg.content = content;
        msg.type = PlMessage.Type.SEQ;

        // Compute sequence number.
        msg.sequence = outstandingSeqsUnackedByThem.isEmpty() ? lastSeqSentByMe + 1 : Collections.max(outstandingSeqsUnackedByThem.keySet()) + 1;
        
        // Put the message in the unackedByThem queue.
        // It will be processed and sent later in the `handleSendToBottomOpportunity()` method.
        outstandingSeqsUnackedByThem.put(msg.sequence, msg);

        // Log "broadcast". For grading purposes.
        logBroadcast(msg);
    }
    
    @Override
    public void handleDeliveryFromBottom(byte[] bottomMessage, SocketAddress bottomAddressSource) {
        // Perfect links are peer-to-peer. The source address can only be our remote host, so we don't use the 2nd parameter.
        PlMessage message = PlMessage.deser(bottomMessage);

        switch (message.type) {
            case SEQ:
                debug("in: SEQ " + message.sequence + " " + Arrays.toString(message.content));

                // Put the message in the unackedByMe queue.
                // It will be processed and acked later, in the `handleSendToBottomOpportunity()` method.
                if (message.sequence > lastAckSentByMe) {
                    outstandingSeqsUnackedByMe.put(message.sequence, message);
                }
                sendingAckAllowed = true;
                break;

            case ACK:
                debug("in: ACK " + message.sequence);
                // Update the state.
                boolean duplicateAck = lastAckReceivedByMe == message.sequence;
                lastAckReceivedByMe = Math.max(lastAckReceivedByMe, message.sequence);

                // Remove the corresponding SEQ(s) from the unackedByThem queue. No need to retransmit it(/them) anymore.
                outstandingSeqsUnackedByThem.keySet().removeIf(seq -> seq <= lastAckReceivedByMe);
                
                if (duplicateAck) {
                    
                    // Maybe some reordering happened. Wait and see.
                    repetitionsOfSameAck += 1;
                    if (repetitionsOfSameAck >= 3) {

                        // Oh no. A packet was likely dropped. Retransmit quickly, ignoring its previous timeout.
                        PlMessage plm = outstandingSeqsUnackedByThem.get(message.sequence + 1);
                        plm.timeout = null;
                        outstandingSeqsUnackedByThem.put(message.sequence + 1, plm);
                        repetitionsOfSameAck = 0;
                        penalizeLightly(message.sequence + 1);

                    }

                } else {
                    // We progressed. Good.
                    repetitionsOfSameAck = 0;
                    encourage();
                }
                break;
        }
    }

    @Override
    public Map<SocketAddress, List<byte[]>> handleSendToBottomOpportunity() {
        List<byte[]> messagesToSendToBottom = new ArrayList<>();

        // Pull down messages from layer on top
        Map<Host, List<byte[]>> messagesFromTop = layerOnTop.handleSendToBottomOpportunity();
        if (messagesFromTop.containsKey(remote)) {
            messagesFromTop.get(remote).stream()
                .forEach((content) -> {
                    broadcast(content);
                });
        }

        // Send SEQs if we have oustanding SEQs that haven't been acked by the remote. (We may have sent them once already or not!)
        if (!outstandingSeqsUnackedByThem.isEmpty()) {

            // Select only the outstanding SEQs that fit in the sending window AND which are due for a (re)transmit.
            Map<Integer, PlMessage> newSeqsToSend = outstandingSeqsUnackedByThem.entrySet().stream()
                .filter(e -> e.getKey() <= lastAckReceivedByMe + sendingWindowSize)
                .filter(e -> e.getValue().timeout == null)
                .collect(Collectors.toMap(
                    e -> e.getKey(),
                    e -> e.getValue()
                ));

            Map<Integer, PlMessage> oldSeqsToResend = outstandingSeqsUnackedByThem.entrySet().stream()
                .filter(e -> e.getKey() <= lastAckReceivedByMe + sendingWindowSize)
                .filter(e -> (
                    e.getValue().timeout != null 
                    && (e.getValue().timeout.compareTo(Instant.now()) < 0)
                ))
                .collect(Collectors.toMap(
                    e -> e.getKey(),
                    e -> e.getValue()
                ));

            // Check for timeouts and penalize sending window if needed.
            if (!oldSeqsToResend.isEmpty()) {
                penalize(oldSeqsToResend.keySet().iterator().next());
            }

            if (!newSeqsToSend.isEmpty() || !oldSeqsToResend.isEmpty()) {
                // Update the state and send down.
                if (!newSeqsToSend.isEmpty()) {
                    lastSeqSentByMe = Math.max(Collections.max(newSeqsToSend.keySet()), lastSeqSentByMe);
                }
                Stream.concat(newSeqsToSend.values().stream(), oldSeqsToResend.values().stream())
                    .forEach(plm -> {
                        try {
                            plm.timeout = Instant.now().plus(sendingSeqTimeout);
                        } catch (DateTimeException dte) {
                            // This will eventually happen with dead processes;
                            // We might as well kill the link here though >_>
                            plm.timeout = Instant.MAX;
                        }

                        debug("out: SEQ " + plm.sequence + " " + Arrays.toString(plm.content));
                        outstandingSeqsUnackedByThem.put(plm.sequence, plm);
                        messagesToSendToBottom.add(plm.ser());
                    });
            }

        }

        // Send ACKs if we have received SEQs from the remote AND the cooldown has elapsed.
        if (
            !outstandingSeqsUnackedByMe.isEmpty() 
            && Duration.between(lastAckSentByMeTimestamp, Instant.now()).compareTo(sendingAckCooldown) > 0
            && sendingAckAllowed
        ) {
            
            sendingAckAllowed = false;  // Do not keep sending ACKs if you have unordered messages here. This floods the network. Let the other guy timeout.

            // What sequence number should my ACK have?
            List<Integer> sequenceNumbersInOrder = outstandingSeqsUnackedByMe.keySet().stream().sorted().collect(Collectors.toList());
            final Integer sequenceNumberOfTheAck = firstDiscontinuity(lastSeqReceivedByMe, sequenceNumbersInOrder);
            
            //System.err.println("("+remote.getId()+") sending ACK "+sequenceNumberOfTheAck+"; lastAckSent="+lastAckSentByMe+"; outstandingSeqsUnackedByMe="+outstandingSeqsUnackedByMe.keySet());

            // Craft the ACK message and send it down.
            PlMessage ackIWillSend = new PlMessage();
            ackIWillSend.sequence = sequenceNumberOfTheAck;
            ackIWillSend.type = PlMessage.Type.ACK;
            debug("out: ACK " + ackIWillSend.sequence);
            messagesToSendToBottom.add(ackIWillSend.ser());

            // Deliver the appropriate SEQs we received. (Appropriate = we have acked it AND we didn't deliver it already)
            outstandingSeqsUnackedByMe.entrySet().stream()
                .filter(e -> e.getKey() <= sequenceNumberOfTheAck && e.getKey() > lastSeqReceivedByMe)  // don't want to deliver twice!
                .forEach((e) -> {
                    if (layerOnTop != null) {
                        debug("deliverup: SEQ " + e.getValue().sequence + " " + Arrays.toString(e.getValue().content));
                        layerOnTop.handleDeliveryFromBottom(e.getValue().content, remote);
                    }
                    logDelivery(e.getValue(), remote.getId());
                });

            // Remove from the queue the appropriate SEQs we received. (Appropriate = we have acked it)
            outstandingSeqsUnackedByMe.keySet().removeIf(seq -> seq <= sequenceNumberOfTheAck);

            // Update the state.
            lastSeqReceivedByMe = sequenceNumberOfTheAck;  // ← these two might be redundant ?
            lastAckSentByMe = sequenceNumberOfTheAck;      // ← ... nah. the names explain different ideas which just 
                                                           // happen to be equivalent in this context. Let's not merge them.
            lastAckSentByMeTimestamp = Instant.now();

        }

        Map<SocketAddress, List<byte[]>> ret = new HashMap<>();
        ret.put(remote.asSocketAddress(), messagesToSendToBottom);
        return ret;
    }

    /** This method takes a sorted list of integers, and sends the last member of the first continuous segment of integers.
     * The prefix parameter is considered as the first element of the list.
     *  e.g. for the list [1, 2, 3, 4, 6, 7, 8, 10] and prefix 0, it would return 4. 
     *  e.g. for the list [4, 5, 7] and prefix 0, it would return 0 */
    private Integer firstDiscontinuity(Integer prefix, List<Integer> in) {
        Integer prev = in.get(0);

        if (prev != prefix + 1) {
            return prefix;
        }

        Integer ret = in.get(in.size() - 1);  // if no discontinuities are found later, return the last member of the list.

        for (int i = 1; i < in.size(); i++) {  // TODO: check for off-by-ones here
            
            Integer curr = in.get(i);
            if (curr != prev + 1) {  // discontinuity detected! return.
                ret = prev;
                break;
            }

            prev = curr;
        }

        return ret;
    }
}
