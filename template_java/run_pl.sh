#!/bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 HOST-ID" >&2
  exit 1
fi

./run.sh --id $1 --hosts ../example/hosts --output ../example/output/pl_${1}.txt ../example/configs/perfect-links.config

